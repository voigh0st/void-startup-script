#!/bin/bash

############################################################
#       This will update/install the newest packages.      #
#         Only use this after first update & reboot        #
############################################################
# 
# Run this script as root after verifying packages & making modifications
#

xbps-install -Syu && xbps-install -Syu && xbps-install -Syu && xbps-install -Syu
xbps-install -Sy void-repo-nonfree void-repo-multilib void-repo-multilib-nonfree
xbps-install -Sy nvidia libpulseaudio-32bit libtxc_dxtn-32bit fontconfig-32bit libavcodec-32bit libavformat-32bit libavresample-32bit libavutil-32bit elogind xdg-desktop-portal xdg-desktop-portal-gtk xdg-user-dirs xdg-user-dirs-gtk xdg-utils Vulkan-Headers Vulkan-Tools vkd3d vulkan-loader emacs-gtk3 calibre neovim youtube-viewer hexchat xfce4-whiskermenu-plugin xfce4-screenshooter mono lutris flatpak vscode zathura-pdf-mupdf zathura-djvu zathura-cb alsa-utils xfce4-pulseaudio-plugin go-mtpfs gvfs-gphoto2 jmtpfs mtpfs simple-mtpfs steam vulkan-loader-32bit nvidia-libs-32bit pass keepassxc git gpg flex

# Removes unecessary services for desktop & prepares polkit
rm /var/service/agetty-tty3 && rm /var/service/agetty-tty4 && rm /var/service/agetty-tty5 && rm /var/service/agetty-tty6 && rm /var/service/sshd && rm /var/service/dhcpcd
ln -srf /etc/sv/{dbus,polkitd,elogind} /var/service && ln -sv /etc/sv/polkitd/ /var/service && ln -sv /etc/sv/dbus/ /var/service

# Remove orphan packages
xbps-remove -o && xbps-remove -o && xbps-remove -o && xbps-install && xbps-remove -o

# Compile WoW64 (32-bit support) for 64-bit Wine
xbps-install -Syu base-devel
xbps-install -Sy libX11-devel-32bit freetype-devel-32bit libjpeg-turbo-devel-32bit gnutls-devel-32bit dbus-devel-32bit libopenal-devel-32bit gstreamer-devel-32bit libxml2-devel-32bit libxslt-devel-32bit tiff-devel-32bit libldap-devel-32bit libXrender-devel-32bit libXrandr-devel-32bit libXfixes-devel-32bit libpulseaudio-32bit libOSMesa-32bit fontconfig-devel-32bit libmpg123-32bit lcms2-devel-32bit libXinerama-devel-32bit libXcomposite-devel-32bit libXcursor-devel-32bit libXi-devel-32bit libsane-32bit glu-devel-32bit libgphoto2-devel-32bit opencl-headers pulseaudio-devel-32bit libpcap-devel-32bit libcups-32bit vkd3d-devel-32bit vulkan-loader-32bit ncmpcpp mpd libX11-devel freetype-devel libjpeg-turbo-devel gnutls-devel dbus-devel libopenal-devel gstreamer1-devel libxml2-devel libxslt-devel tiff-devel libldap-devel libXrender-devel libXrandr-devel libXfixes-devel libpulseaudio libOSMesa fontconfig-devel libmpg123 lcms2-devel libXinerama-devel libXcomposite-devel libXcursor-devel libXi-devel libsane glu-devel libgphoto2-devel opencl-headers pulseaudio-devel libpcap-devel vkd3d-devel vulkan-loader cross-i686-w64-mingw32-crt cross-i686-w64-mingw32 cross-x86_64-w64-mingw32 libXcursor-devel libXi-devel libxshmfence-devel libXxf86vm-devel libXrandr-devel libXrandr-devel-32bit libXfixes-devel-32bit libXinerama-devel-32bit libXcomposite-devel-32bit libglusterfs-32bit libOSMesa-32bit libpcap-devel-32bit libdbus-c++-devel-32bit libsane-32bit libgphoto2-devel-32bit gstreamer1-devel-32bit libgudev-devel-32bit libcups-32bit fontconfig-devel-32bit libgsm-devel-32bit libmpg123-32bit libopenal-devel-32bit vkd3d-devel-32bit libldap-devel-32bit libXrender-devel-32bit libxml2-devel-32bit libxslt-devel-32bit libjpeg-turbo-devel-32bit FAudio-devel gst-plugins-base1-devel mit-krb5-devel mpg123-devel glu-devel-32bit ncurses-devel-32bit SDL2-devel-32bit mit-krb5-devel-32bit gnutls-devel-32bit mpg123-devel-32bit libX11-devel freetype-devel libjpeg-turbo-devel gnutls-devel dbus-devel libopenal-devel gstreamer1-devel libxml2-devel libxslt-devel tiff-devel libldap-devel xkill htop libXrender-devel libXrandr-devel libXfixes-devel libpulseaudio libOSMesa fontconfig-devel libmpg123 lcms2-devel libXinerama-devel libXcomposite-devel libXcursor-devel libXi-devel libsane glu-devel libgphoto2-devel opencl-headers pulseaudio-devel libpcap-devel vkd3d-devel
mkdir ~/wine64
mkdir ~/wine32
mkdir ~/wine
cd wine && git clone git://source.winehq.org/git/wine.git
cd ~/wine64
../wine/configure --enable-win64
make -j5

# There may be an issue with installing some flatpak packages that causes this script to fail. 
flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
flatpak install flathub com.jagex.RuneScape
flatpak install flathub com.discordapp.Discord
flatpak install flathub com.mojang.Minecraft

# exit # This exits the root env
# reboot 
